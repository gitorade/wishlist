unit FormMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  EditBtn, Spin, ExtCtrls,
  SQLDB, SQLite3Conn, DateUtils,
  DBUtil;

type

  { TMainForm }

  TMainForm = class(TForm)
    BoxAddPerson		: TGroupBox;
    BoxBirthDate		: TGroupBox;
    BuAddPerson			: TButton;
    BuDeletePerson: TButton;
    BuClearPerson: TButton;
    BuAddWish: TButton;
    BuAddGift: TButton;
    BuDeleteWish: TButton;
    BuUpdatePerson: TButton;
    CbBirthDay			: TComboBox;
    CbBirthMonth		: TComboBox;
    CbPerson: TComboBox;
    CheckBirthYear	: TCheckBox;
    CbWishPriority: TComboBox;
    CbPersonWishes: TComboBox;
    EdGiftDate: TDateEdit;
    EdBirthYear			: TSpinEdit;
    EdFirstName			: TEdit;
    EdMaidenName		: TEdit;
    EdLastName			: TEdit;
    EdPhone					: TEdit;
    EdBirthPlace		: TEdit;
    EdEMail					: TEdit;
    BoxBasicInfo		: TGroupBox;
    BoxContactInfo	: TGroupBox;
    BoxAddWish: TGroupBox;
    BoxAddGift: TGroupBox;
    LaUpcomingBirthdays: TLabel;
    LaGiftDate: TLabel;
    LaGiftPrice: TLabel;
    LaGiftWish: TLabel;
    LaWish: TLabel;
    LaWishPerson: TLabel;
    LaWishPriority: TLabel;
    LaBirthDay			: TLabel;
    LaBirthMonth		: TLabel;
    LaBirthPlace		: TLabel;
    LaEMail					: TLabel;
    LaFirstName			: TLabel;
    LaLastName			: TLabel;
    LaMaidenName		: TLabel;
    LaPhone					: TLabel;
    LbPerson: TListBox;
    LbUpcomingBirthdays: TListBox;
    MemoPersonWishes: TMemo;
    MemoWish: TMemo;
    PageControl			: TPageControl;
    EdGiftPrice: TSpinEdit;
    StatusBar: TStatusBar;
    TabHome					: TTabSheet;
    TabPerson				: TTabSheet;
    TabWish: TTabSheet;
    procedure BuAddGiftClick(Sender: TObject);
    procedure BuAddPersonClick(Sender: TObject);
    procedure BuAddWishClick(Sender: TObject);
    procedure BuClearPersonClick(Sender: TObject);
    procedure BuDeletePersonClick(Sender: TObject);
    procedure BuDeleteWishClick(Sender: TObject);
    procedure BuUpdatePersonClick(Sender: TObject);
    procedure CbPersonChange(Sender: TObject);
    procedure CheckBirthYearChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure LbPersonSelectionChange(Sender: TObject; User: boolean);
    procedure StatusBarClick(Sender: TObject);
  private
    procedure InitGUI;
    procedure InitDB;
    procedure ClearPersonInfo;
    procedure LoadDataForDisplay;
    procedure ResetStatusBar;
    procedure LoadPersonWishesForDisplay;
    procedure LoadPersonForDisplay;

	private
    DBConn	: TSQLite3Connection;
    DBTrans	: TSQLTransaction;

  public

  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
begin
	// This stuff is required to ensure proper use of ISO dates YYYY-mm-dd
  DefaultSQLFormatSettings.DateSeparator := '-';
  DefaultFormatSettings.DateSeparator := '-';
  DefaultSQLFormatSettings.ShortDateFormat := 'YYYY-mm-dd';
  DefaultFormatSettings.ShortDateFormat := 'YYYY-mm-dd';

  InitDB;
  InitGUI;

  PageControl.ActivePageIndex := 0;
end;

procedure TMainForm.LbPersonSelectionChange(Sender: TObject; User: boolean);
begin
  LoadPersonForDisplay;
end;

procedure TMainForm.StatusBarClick(Sender: TObject);
begin
	ResetStatusBar;
end;

procedure TMainForm.CheckBirthYearChange(Sender: TObject);
begin
  EdBirthYear.Enabled := CheckBirthYear.Checked;
end;

procedure TMainForm.BuAddPersonClick(Sender: TObject);
var
	query			: TSQLQuery;
  builder		:	TQueryBuilder;
  birthdate	:	TDateTime;
begin
  try
    ResetStatusBar;
    query 	:= TSQLQuery.Create(DBTrans);
    query.Transaction := DBTrans;
    builder := TQueryBuilder.Create;

    // Build query to insert into person
  	// MessageDlg('Add Person', 'Database connection established', mtInformation, [mbOK], 0);

    if Trim(EdFirstName.Text).IsEmpty then begin
      MessageDlg('Add Person - Error', 'Please provide at least the first name', mtError, [mbOK], 0);
    	Exit;
    end else
    	builder.AddParam('first_name', Trim(EdFirstName.Text));

		if (CbBirthDay.ItemIndex >= 0) and (CbBirthMonth.ItemIndex >= 0) then
    	begin
        birthdate := EncodeDate(EdBirthYear.Value, CbBirthMonth.ItemIndex+1, CbBirthDay.ItemIndex+1);
			  if CheckBirthYear.Checked then
          builder.AddParam('birthday', FormatDateTime('YYYY-mm-dd', birthdate))
        else
      	  builder.AddParam('birthday', FormatDateTime('mm-dd', birthdate));
    	end;

    builder.AddParam('maiden_name', Trim(EdMaidenName.Text));
    builder.AddParam('last_name', Trim(EdLastName.Text));
    builder.AddParam('birth_place', Trim(EdBirthPlace.Text));
    if Trim(EdEMail.Text).Length > 0 then // UNIQUE
      builder.AddParam('email', Trim(EdEMail.Text));
    if Trim(EdPhone.Text).Length > 0 then // UNIQUE
      builder.AddParam('phone', Trim(EdPhone.Text));
    builder.PrepareInsert(query, 'person');

    try
      query.ExecSQL;
      DBTrans.Commit;
		except
    	on E: ESQLDatabaseError do
    		MessageDlg('Add Person - Error', 'There was an error when trying to insert the record (make sure first name, e-mail and phone no. are unique).', mtError, [mbOK], 0);
      else
        raise
    end;

  StatusBar.Color := clGreen;
  StatusBar.SimpleText := 'Person added successfully';
	LoadDataForDisplay;

  finally
    FreeAndNil(builder)
  end;

end;

procedure TMainForm.BuAddGiftClick(Sender: TObject);
var
  personId: Integer;
  wishId: Integer;
  builder: TQueryBuilder;
  query: TSQLQuery;
  dateStr: String;
  price: Integer;
begin
	if (CbPerson.ItemIndex < 0) or (CbPersonWishes.ItemIndex < 0) then
    begin
      ShowMessage('Please select a person and a wish first');
      exit;
    end;

  try
		query := TSQLQuery.Create(nil);
    builder := TQueryBuilder.Create;

    wishId := PtrUInt(CbPersonWishes.Items.Objects[CbPersonWishes.itemIndex]);
    personId := PtrUInt(CbPerson.Items.Objects[CbPerson.ItemIndex]);
    price := EdGiftPrice.Value;

    builder.AddParam('person_id', personId);
    builder.AddParam('wish_id', wishId);
    builder.AddParam('date_gifted', dateStr);
    builder.AddParam('price', price);

  finally
    FreeAndNil(query);
    FreeAndNil(builder);
  end;
end;

procedure TMainForm.BuAddWishClick(Sender: TObject);
var
  query: TSQLQuery;
  builder: TQueryBuilder;
  id: Integer;
begin
	ResetStatusBar;

  if CbPerson.ItemIndex < 0 then
    begin
			ShowMessage('Please select a person first');
      exit;
    end;

  if Trim(MemoWish.Text).Length = 0 then
    begin
      ShowMessage('Please enter a wish first');
      exit;
    end;

  try
		query := TSQLQuery.Create(DBTrans);
    builder := TQueryBuilder.Create;
    query.Transaction := DBTrans;

    id := PtrUInt(CbPerson.Items.Objects[CbPerson.ItemIndex]);
    builder.AddParam('person_id', id);
    builder.AddParam('description', Trim(MemoWish.Text));
    builder.AddParam('priority', CbWishPriority.ItemIndex + 1);
    builder.PrepareInsert(query, 'wish');

    try
      query.ExecSQL;
      DBTrans.Commit
    except
    	on E: ESQLDatabaseError do
				MessageDlg('Add Wish - Error', 'There was an error when trying to insert the record', mtError, [mbOK], 0);
      else
      	raise
    end;

    LoadPersonWishesForDisplay;
    StatusBar.Color := clGreen;
    StatusBar.SimpleText := 'Wish added successfully';

  finally
    FreeAndNil(query);
    FreeAndNil(builder);
  end;
end;

procedure TMainForm.BuClearPersonClick(Sender: TObject);
begin
  ClearPersonInfo;
end;

procedure TMainForm.BuDeletePersonClick(Sender: TObject);
var
	query		: TSQLQuery;
  id			: Integer;
begin
  if LbPerson.ItemIndex < 0 then
    exit;

  ResetStatusBar;
  if MessageDlg('Delete Person', 'Are you sure you want to delete "' + LbPerson.Items[LbPerson.ItemIndex] + '"?', mtConfirmation, mbYesNo, 0) <> mrYes then
    exit;

  try
    query 	:= TSQLQuery.Create(DBTrans);
    query.Transaction := DBTrans;
    query.SQL.Text := 'DELETE FROM person WHERE rowid=:rowid;';
    id := PtrUInt(LbPerson.Items.Objects[LbPerson.ItemIndex]);
    query.ParamByName('rowid').AsInteger := id;
    try
      query.ExecSQL;
      DBTrans.Commit;
		except
    	on E: ESQLDatabaseError do
    		MessageDlg('Delete Person - Error', 'There was an error when trying to delete the record.', mtError, [mbOK], 0);
      else
        raise
    end;

    StatusBar.SimpleText := 'Entry deleted successfully';
	  LoadDataForDisplay;
  finally
    FreeAndNil(query);
  end;
end;

procedure TMainForm.BuDeleteWishClick(Sender: TObject);
var
	query		: TSQLQuery;
  id			: Integer;
begin
  ResetStatusBar;

  if CbPersonWishes.ItemIndex < 0 then
    exit;

  if MessageDlg('Delete Wish', 'Are you sure you want to delete "' + CbPersonWishes.Items[CbPersonWishes.ItemIndex] + '"?', mtConfirmation, mbYesNo, 0) <> mrYes then
    exit;

  try
     query 	:= TSQLQuery.Create(DBTrans);
     query.Transaction := DBTrans;
     query.SQL.Text := 'DELETE FROM wish WHERE rowid=:rowid;';
     id := PtrUInt(CbPersonWishes.Items.Objects[CbPersonWishes.ItemIndex]);
     query.ParamByName('rowid').AsInteger := id;
     try
       query.ExecSQL;
       DBTrans.Commit;
 		except
     	on E: ESQLDatabaseError do
     		MessageDlg('Delete Wish - Error', 'There was an error when trying to delete the record.', mtError, [mbOK], 0);
       else
         raise
     end;

     LoadPersonWishesForDisplay;
     StatusBar.SimpleText := 'Entry deleted successfully';
   finally
     FreeAndNil(query);
   end;
end;

procedure TMainForm.BuUpdatePersonClick(Sender: TObject);
var
  id			: Integer;
  builder	: TQueryBuilder;
  query		: TSQLQuery;
  birthday : TDateTime;
begin
  if LbPerson.ItemIndex < 0 then
      exit;
  ResetStatusBar;
  try
    builder := TQueryBuilder.Create;
    query := TSQLQuery.Create(DBTrans);
    query.Transaction := DBTrans;
    id := PtrUInt(LbPerson.Items.Objects[LbPerson.ItemIndex]);
		builder.AddParam('rowid', id, ptWhere);
    builder.AddParam('last_name', Trim(EdLastName.Text));
    if Trim(EdEMail.Text).Length > 0 then // UNIQUE
      builder.AddParam('email', Trim(EdEMail.Text))
    else
      builder.AddParam('email', Null);
    if Trim(EdPhone.Text).Length > 0 then // UNIQUE
      builder.AddParam('phone', Trim(EdPhone.Text))
    else
      builder.AddParam('phone', Null);
    if (CbBirthDay.ItemIndex >= 0) and (CbBirthMonth.ItemIndex >= 0) then
      begin
        birthday := EncodeDate(EdBirthYear.Value, CbBirthMonth.ItemIndex+1, CbBirthDay.ItemIndex+1);
        if CheckBirthYear.Checked then
          builder.AddParam('birthday', FormatDateTime('YYYY-mm-dd', birthday))
        else
          builder.AddParam('birthday', FormatDateTime('mm-dd', birthday));
      end;

    builder.Where := 'rowid=:rowid';
    builder.PrepareUpdate(query, 'person');
    try
			query.ExecSQL;
      DBTrans.Commit;
		except
    	on E: ESQLDatabaseError do
    		MessageDlg('Update Person - Error', 'There was an error when trying to update the record.', mtError, [mbOK], 0);
      else
        raise
    end;
    StatusBar.Color := clGreen;
    StatusBar.SimpleText := 'Entry updated successfully';
    LoadPersonForDisplay;
  finally
    FreeAndNil(builder);
    FreeAndNil(query);
  end;
end;

procedure TMainForm.CbPersonChange(Sender: TObject);
begin
  LoadPersonWishesForDisplay;
end;

procedure TMainForm.InitGUI;
var
  i: Integer;
begin
  CbBirthDay.Clear;
  for i:=1 to 31 do
  	CbBirthDay.Items.Add(IntToStr(i));

	CbBirthMonth.Clear;
  CbBirthMonth.Items.Add('Jan');
  CbBirthMonth.Items.Add('Feb');
  CbBirthMonth.Items.Add('Mar');
  CbBirthMonth.Items.Add('Apr');
  CbBirthMonth.Items.Add('May');
  CbBirthMonth.Items.Add('Jun');
  CbBirthMonth.Items.Add('Jul');
  CbBirthMonth.Items.Add('Aug');
  CbBirthMonth.Items.Add('Sep');
  CbBirthMonth.Items.Add('Oct');
  CbBirthMonth.Items.Add('Nov');
  CbBirthMonth.Items.Add('Dec');

  CheckBirthYear.Checked	:= False;
  EdBirthYear.Enabled 		:= False;
  EdBirthYear.MinValue 		:= 1900;
  EdBirthYear.MaxValue 		:= 2100;
  EdBirthYear.Clear;

	LoadDataForDisplay;
end;

procedure TMainForm.InitDB;
begin
  DBConn := TSQLite3Connection.Create(Self);
  DBTrans := TSQLTransaction.Create(DBConn);

  DBConn.DatabaseName := './wishlist.sqlite';
  DBConn.Open;
  DBTrans.Database := DBConn;
end;

procedure TMainForm.ClearPersonInfo;
begin
  EdFirstName.Clear;
  EdMaidenName.Clear;
  EdLastName.Clear;
  EdBirthPlace.Clear;
  EdEMail.Clear;
  EdPhone.Clear;
  CheckBirthYear.Checked := False;
  EdBirthYear.Enabled := False;
  EdBirthYear.Value := EdBirthYear.MinValue;
  CbBirthDay.ItemIndex := -1;
  CbBirthMonth.ItemIndex := -1;
end;

procedure TMainForm.LoadDataForDisplay;
var
	query		: TSQLQuery;
  builder	:	TQueryBuilder;
  id			: TObject;
  dateStr	: String;
  date		: TDateTime;
  y,m,d		: Word;
  now_y		: Word;
  now_m		: Word;
  now_d		: Word;
  remainingDays	: Integer;
  IsYearKnown	: Boolean;
  fullName	: String;
begin
	try
    query 	:= TSQLQuery.Create(DBTrans);
    builder := TQueryBuilder.Create;

    query.Transaction := DBTrans;
		builder.OrderBy := 'first_name COLLATE NOCASE';
    builder.PrepareSelect(query, 'person', 'rowid, *');

    LbPerson.Items.BeginUpdate;
    LbPerson.Clear;
    LbUpcomingBirthdays.Clear;
    CbPerson.Items.BeginUpdate;
    CbPerson.Clear;

    query.UniDirectional := True;
    query.Open;
    query.First;
    while not query.EOF do
      begin
        id := TObject(PtrUInt(query['rowid']));
        fullName := query['first_name'] + ' ' + query['last_name'];
        LbPerson.AddItem(fullName, id);
        CbPerson.AddItem(fullName, id);

        // Add upcoming birthdays to the display:
        if not query.FieldByName('birthday').IsNull then
          begin
            DateStr := query['birthday'];
            date := StrToDate(DateStr);
            // looking for '0001-mm-dd':
            IsYearKnown := (DateStr.Length > 5) and (DateStr.Substring(0, 4) <> '0001');

            DecodeDate(date, y, m, d);
            DecodeDate(Now, now_y, now_m, now_d);

            if (m > now_m) or (m = now_m) and (d > now_d) then
              y := now_y
            else
              y := now_y + 1;

            date := EncodeDate(y, m, d);
            remainingDays := DaysUntilDayOfAYear(date);
            if remainingDays <= 14 then
              LbUpcomingBirthdays.Items.Add(fullName + ':  ' + IntToStr(remainingDays) + '  days remaining');
          end;

        query.Next;
      end;
    query.Close;
    LbPerson.Items.EndUpdate;
    CbPerson.Items.EndUpdate;

    LoadPersonForDisplay;

  finally
    FreeAndNil(builder)
  end;
end;

procedure TMainForm.ResetStatusBar;
begin
  StatusBar.Color := clDefault;
  StatusBar.SimpleText := '';
end;

procedure TMainForm.LoadPersonWishesForDisplay;
var
  id : Integer;
  builder : TQueryBuilder;
  query : TSQLQuery;
begin
  if CbPerson.ItemIndex < 0 then
    exit;

	id := PtrUInt(CbPerson.Items.Objects[CbPerson.ItemIndex]);

  try
    query 	:= TSQLQuery.Create(DBTrans);
    builder := TQueryBuilder.Create;
    query.Transaction := DBTrans;

    builder.Where := 'person_id=:person_id';
    builder.AddParam('person_id', id, ptWhere);
    builder.OrderBy := 'priority';
    builder.PrepareSelect(query, 'wish', 'rowid, *');

    MemoPersonWishes.Clear;
    CbPersonWishes.Clear;
		query.Open;
    query.First;
		while not query.EOF do
      begin
        CbPersonWishes.AddItem(query['description'], TObject(PtrUInt(query['rowid'])));
        MemoPersonWishes.Lines.Add('- ' + query['description'] + sLineBreak + '  (Prio: ' + IntToStr(query['priority']) + ')' + sLineBreak);
				query.Next;
      end;
    query.Close;

    if CbPersonWishes.Items.Count > 0 then
      CbPersonWishes.ItemIndex := 0;

  finally
    FreeAndNil(query);
    FreeAndNil(builder);
  end;
end;

procedure TMainForm.LoadPersonForDisplay;
var
  id			: Integer;
  query		: TSQLQuery;
  builder	:	TQueryBuilder;
  date		: TDateTime;
  dateStr	: String;
  y,m,d		: Word;
  isYearKnown	: Boolean;
begin
	if LbPerson.ItemIndex < 0 then
    exit;
  try
    query := TSQLQuery.Create(Nil);
    query.Transaction := DBTrans;
    builder := TQueryBuilder.Create;
    id := PtrUInt(LbPerson.Items.Objects[LbPerson.ItemIndex]);
    builder.AddParam('rowid', id, ptWhere);
    builder.Where := 'rowid=:rowid';
    builder.PrepareSelect(query, 'person');
    query.Open;
    query.First;
    ClearPersonInfo;
    EdFirstName.Text := query['first_name'];
    EdMaidenName.Text := query['maiden_name'];
    EdLastName.Text := query['last_name'];
    EdBirthPlace.Text := query['birth_place'];
    if not query.FieldByName('email').IsNull then
      EdEMail.Text := query['email'];
    if not query.FieldByName('phone').IsNull then
      EdPhone.Text := query['phone'];

    if not query.FieldByName('birthday').IsNull then
      begin
        dateStr := query['birthday'];
        date := StrToDate(dateStr);
        DecodeDate(date, y, m, d);
        if (d > 0) and (d <= CbBirthDay.Items.Count) then
        	CbBirthDay.ItemIndex := d-1;
        if (m > 0) and (m <= CbBirthMonth.Items.Count) then
          CbBirthMonth.ItemIndex := m-1;
        // looking for '0001-mm-dd':
        IsYearKnown := (dateStr.Length > 5) and (dateStr.Substring(0, 4) <> '0001');
        if IsYearKnown then
          begin
            EdBirthYear.Value := y;
            EdBirthYear.Enabled := True;
          	CheckBirthYear.Checked := True;
          end;
      end;

    query.Close;
  finally
    FreeAndNil(query);
  end;
end;

end.

