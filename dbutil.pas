{ DBUtil

	This unit contains utility classes and functions to be used together with SQL/SQLDB/SQLITE3
}

unit DBUtil;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, DB, SQLDB, SQLite3Conn, Variants, Dialogs,
  DateUtils;

type
	TParamType = (ptWrite, ptWhere);

	TQueryParam = record
    Column		: String;
    Value			: Variant;
    ParamType	: TParamType;
  end;

{ TQueryBuilder Interface

	The goal of this class if to allow dynamic building of SQL queries at runtime, providing:
  	1. The ability to have varying values/columns inserted or retrieved (not hard-coded); and
    2. Simplying client code by capsuling some of the details (e.g., preparing statements)

  Examples:
  	INSERT INTO <table> (colA, colB, colC) VALUES (:colA, :colB, :colC);

    SELECT rowid, * FROM <table> WHERE TRUE;
}
type
  TQueryBuilder = class(TObject)

  procedure AddParam(const AName: String; const AValue: Variant; AType: TParamType = ptWrite);
  procedure SetColumnCount(ACount: Integer);
  procedure PrepareInsert(var AQuery: TSQLQuery; const ATable: String);
  procedure PrepareSelect(var AQuery: TSQLQuery; const ATable: String; const AColumns: String = '*');
  procedure PrepareUpdate(var AQuery: TSQLQuery; const ATable: String);

  private
    columns: Array of String;
    values: Array of Variant;
    params: Array of TQueryParam;
    FWhere: String;
    FOrder: String;
    size: Integer;

  public
    property Where: String read FWhere write FWhere;
    property OrderBy: String read FOrder write FOrder;

  const
	  linearGrowth = 5;
end;

function DaysUntilDayOfAYear(const ADay: TDateTime): Word;

implementation

{ Free helper functions }
function DaysUntilDayOfAYear(const ADay: TDateTime): Word;
begin
  Result := DayOfTheYear(ADay) - DayOfTheYear(Now);
  // Add correction if annual day already was in this year:
  if Result < 0 then
    Result += DaysInYear(Now);
end;

{ TQueryBuilder Implementation }
procedure TQueryBuilder.AddParam(const AName: String; const AValue: Variant; AType: TParamType);
var
  i	:	Integer;
  param	: TQueryParam;
begin
  // Check first if column AName already exists
	for i:=0 to size-1 do
  	if columns[i] = AName then begin
      values[i] := AValue;
      Exit;
    end;

  // Otherwise, add new column
  if size = Length(columns) then
    SetColumnCount(Length(columns) + linearGrowth);
  columns[size] := AName;
  values[size] := AValue;
  param.Column := AName;
  param.Value := AValue;
  param.ParamType := AType;
  params[size] := param;
  size += 1;
end;

procedure TQueryBuilder.SetColumnCount(ACount: Integer);
begin
  if ACount > Length(columns) then begin
    SetLength(columns, ACount);
    SetLength(values, ACount);
    SetLength(params, ACount);
  end;
end;

procedure TQueryBuilder.PrepareInsert(var AQuery: TSQLQuery; const ATable: String);
var
  i: Integer;
  colList: String;
  valList: String;
begin
	if size = 0 then
    Exit;

  colList := ' (' + ''.Join(', ', columns, 0, size) + ')';
  valList := ' (:' + ''.Join(', :', columns, 0, size) + ')';

  AQuery.Clear;
  AQuery.SQL.Text := 'INSERT INTO ' + ATable + colList + ' VALUES' + valList + ';';
  for i:=0 to size-1 do
    AQuery.ParamByName(params[i].Column).Value := params[i].Value;
end;

procedure TQueryBuilder.PrepareSelect(var AQuery: TSQLQuery;
  const ATable: String; const AColumns: String);
var
  i	: Integer;
  queryStr	: String;
begin
  queryStr := 'SELECT ' + AColumns + ' FROM ' + ATable;
  if FWhere.Length > 0 then
    queryStr += ' WHERE ' + FWhere;
  if FOrder.Length > 0 then
    queryStr += ' ORDER BY ' + FOrder;

  AQuery.Clear;
  AQuery.SQL.Text := queryStr + ';';
  for i:=0 to size-1 do
    AQuery.ParamByName(params[i].Column).Value := params[i].Value;
end;

procedure TQueryBuilder.PrepareUpdate(var AQuery: TSQLQuery; const ATable: String);
var
  i	: Integer;
  queryString	: String;
  setStr	: String;
begin
  if size = 0 then
      exit;

  queryString := 'UPDATE ' + ATable + ' SET ';
  for i:=0 to size-1 do
    if params[i].ParamType = ptWrite then
        queryString += params[i].Column + '=:' + params[i].Column + ',';
  SetLength(queryString, queryString.Length-1); // Remove trailing ',' until cleaner function is written

  AQuery.Clear;
  AQuery.SQL.Text := queryString + ' WHERE ' + FWhere + ';';
  for i:=0 to size-1 do
    if not VarIsNull(params[i].Value) then
    	AQuery.ParamByName(params[i].Column).Value := params[i].Value;
end;

end.

